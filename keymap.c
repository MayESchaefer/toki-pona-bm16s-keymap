#include QMK_KEYBOARD_H

enum layers {
	BASE,
	SP1,
	SP2,
	A,
	E,
	IKE,
	JAN,
	KAMA,
		KILI,
		KU,
	LA,
		LI,
		LEN,
	MI,
		MONSI,
	NIMI,
	O,
	PI,
		PINI,
		POKA,
	SINA,
		SEWI,
		SUWI,
	TAWA,
	UTALA,
	WILE
};

enum custom_keycodes {
	TP_A = SAFE_RANGE, TP_ALE, TP_ALI, TP_AKESI, TP_ALA, TP_ANPA, TP_APEJA, TP_ALASA, TP_ANTE, TP_ANU, TP_AWEN, TP_E, TP_EN, TP_EPIKU, TP_ESUN, TP_IKE, TP_IJO, TP_ILO, TP_INSA, TP_JAKI, TP_JELO, TP_JAN, TP_JO, TP_JASIMA, TP_KALA, TP_KEN, TP_KILI, TP_KIN, TP_KIPISI, TP_KIJETESANTAKALU, TP_KAMA, TP_KULUPU, TP_KALAMA, TP_KON, TP_KO, TP_KEPEKEN, TP_KASI, TP_KUTE, TP_KU, TP_KULE, TP_KIWEN, TP_LAPE, TP_LOJE, TP_LUKA, TP_LA, TP_LILI, TP_LUKIN, TP_LON, TP_LUPA, TP_LASO, TP_LETE, TP_LIPU, TP_LAWA, TP_LI, TP_LINLUWI, TP_LINJA, TP_LEN, TP_LEKO, TP_LANPAN, TP_MAMA, TP_MELI, TP_MANI, TP_MIJE, TP_MOKU, TP_MOLI, TP_MI, TP_MUN, TP_MESO, TP_MUSI, TP_MUTE, TP_MU, TP_MA, TP_MISIKEKE, TP_MONSUTA, TP_MONSI, TP_NANPA, TP_NENA, TP_NI, TP_NAMAKO, TP_NIMI, TP_NOKA, TP_NASIN, TP_NASA, TP_N, TP_OKO, TP_OLIN, TP_ONA, TP_O, TP_OPEN, TP_PAN, TP_PAKE, TP_PAKALA, TP_PALI, TP_PIMEJA, TP_PANA, TP_PI, TP_PALISA, TP_PU, TP_PINI, TP_PILIN, TP_PIPI, TP_POKI, TP_PONA, TP_POKA, TP_POWE, TP_SAMA, TP_SIN, TP_SIJELO, TP_SIKE, TP_SULI, TP_SEME, TP_SONA, TP_SOKO, TP_SINPIN, TP_SINA, TP_SITELEN, TP_SOWELI, TP_SEWI, TP_SELI, TP_SELO, TP_SUNO, TP_SUPA, TP_SUWI, TP_TAN, TP_TELO, TP_TONSI, TP_TOMO, TP_TENPO, TP_TOKI, TP_TASO, TP_TAWA, TP_TU, TP_UNPA, TP_UTA, TP_UTALA, TP_WAWA, TP_WEKA, TP_WALO, TP_WAN, TP_WASO, TP_WILE,

	TP_BSP, TP_ENT,

	TP_DOT, TP_COMM, TP_QUOT, TP_EXCL, TP_QUES, TP_DASH, TP_UNDR, TP_SCLN, TP_CLON, TP_LBRC, TP_RBRC,

	TP_SPC, TP_BSPC,

	TPL_A, TPL_E, TPL_I, TPL_J, TPL_K, TPL_L, TPL_M, TPL_N, TPL_O, TPL_P, TPL_S, TPL_T, TPL_U, TPL_W
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[BASE] = LAYOUT_ortho_4x4(
		LT(SP1,TP_BSP),	TG(A),		TG(E),		TG(IKE),
		LT(SP2,TP_ENT),	TG(JAN),	TG(KAMA),	TG(LA),
		TG(MI),			TG(NIMI),	TG(O),		TG(PI),
		TG(SINA),		TG(TAWA),	TG(UTALA),	TG(WILE)
	),
	[SP1] = LAYOUT_ortho_4x4(
		_______,	TP_DOT,		TP_COMM,	TP_QUOT,
		_______,	TP_EXCL,	TP_QUES,	TP_DASH,
		TP_SPC,		TP_UNDR,	TP_SCLN,	TP_CLON,
		TP_BSPC,	TP_LBRC,	TP_RBRC,	OSM(MOD_LSFT)
	),
	[SP2] = LAYOUT_ortho_4x4(
		TP_SPC,		TPL_A,		TPL_E,		TPL_I,
		_______,	TPL_J,		TPL_K,		TPL_L,
		TPL_M,		TPL_N,		TPL_O,		TPL_P,
		TPL_S,		TPL_T,		TPL_U,		TPL_W
	),
	[A] = LAYOUT_ortho_4x4(
		_______,	TP_A,		TP_ALE,		TP_ALI,
		_______,	_______,	TP_AKESI,	TP_ALA,
		_______,	TP_ANPA,	_______,	TP_APEJA,
		TP_ALASA,	TP_ANTE,	TP_ANU,		TP_AWEN
	),
	[E] = LAYOUT_ortho_4x4(
		_______,	_______,	TP_E,		_______,
		_______,	_______,	_______,	_______,
		_______,	TP_EN,		_______,	TP_EPIKU,
		TP_ESUN,	_______,	_______,	_______
	),
	[IKE] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	TP_IKE,
		_______,	TP_IJO,		_______,	TP_ILO,
		_______,	TP_INSA,	_______,	_______,
		_______,	_______,	_______,	_______
	),
	[JAN] = LAYOUT_ortho_4x4(
		_______,	TP_JAKI,	TP_JELO,	_______,
		_______,	TP_JAN,		_______,	_______,
		_______,	_______,	TP_JO,		_______,
		TP_JASIMA,	_______,	_______,	_______
	),
	[KAMA] = LAYOUT_ortho_4x4(
		_______,	TP_KALA,				TP_KEN,		TG(KILI),
		_______,	TP_KIJETESANTAKALU,		TP_KAMA,	TP_KULUPU,
		TP_KALAMA,	TP_KON,					TP_KO,		TP_KEPEKEN,
		TP_KASI,	TP_KUTE,				TG(KU),		TP_KIWEN
	),
	[KILI] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	TP_KILI,
		_______,	_______,	_______,	_______,
		_______,	TP_KIN,		_______,	TP_KIPISI,
		_______,	_______,	_______,	_______
	),
	[KU] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	_______,
		_______,	_______,	_______,	TP_KULE,
		_______,	_______,	_______,	_______,
		_______,	_______,	TP_KU,		_______
	),
	[LA] = LAYOUT_ortho_4x4(
		_______,	TP_LAPE,	TG(LEN),	TG(LI),
		_______,	TP_LOJE,	TP_LUKA,	TP_LA,
		_______,	TP_LUKIN,	TP_LON,		TP_LUPA,
		TP_LASO,	TP_LETE,	TP_LIPU,	TP_LAWA
	),
	[LI] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	TP_LI,
		_______,	_______,	_______,	TP_LILI,
		_______,	TP_LINJA,	_______,	_______,
		_______,	_______,	_______,	TP_LINLUWI
	),
	[LEN] = LAYOUT_ortho_4x4(
		_______,	_______,	TP_LEN,		_______,
		_______,	_______,	TP_LEKO,	_______,
		_______,	TP_LANPAN,	_______,	_______,
		_______,	_______,	_______,	_______
	),
	[MI] = LAYOUT_ortho_4x4(
		_______,	TP_MAMA,	TP_MELI,	TP_MANI,
		_______,	TP_MIJE,	TP_MOKU,	TP_MOLI,
		TP_MI,		TP_MUN,		TG(MONSI),	TP_MESO,
		TP_MUSI,	TP_MUTE,	TP_MU,		TP_MA
	),
	[MONSI] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	TP_MISIKEKE,
		_______,	_______,	_______,	_______,
		_______,	TP_MONSUTA,	TP_MONSI,	_______,
		_______,	_______,	_______,	_______
	),
	[NIMI] = LAYOUT_ortho_4x4(
		_______,	TP_NANPA,	TP_NENA,	TP_NI,
		_______,	_______,	_______,	_______,
		TP_NAMAKO,	TP_NIMI,	TP_NOKA,	TP_NASIN,
		TP_NASA,	TP_N,	_______,	_______
	),
	[O] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	_______,
		_______,	_______,	TP_OKO,		TP_OLIN,
		_______,	TP_ONA,		TP_O,		TP_OPEN,
		_______,	_______,	_______,	_______
	),
	[PI] = LAYOUT_ortho_4x4(
		_______,	TP_PAN,		TP_PAKE,	TG(PINI),
		_______,	_______,	TP_PAKALA,	TP_PALI,
		TP_PIMEJA,	TP_PANA,	TG(POKA),	TP_PI,
		TP_PALISA,	_______,	TP_PU,	_______
	),
	[PINI] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	TP_PINI,
		_______,	_______,	_______,	TP_PILIN,
		_______,	_______,	_______,	TP_PIPI,
		_______,	_______,	_______,	_______
	),
	[POKA] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	_______,
		_______,	_______,	TP_POKI,	_______,
		_______,	TP_PONA,	TP_POKA,	_______,
		_______,	_______,	_______,	TP_POWE
	),
	[SINA] = LAYOUT_ortho_4x4(
		_______,	TP_SAMA,	TG(SEWI),	TP_SIN,
		_______,	TP_SIJELO,	TP_SIKE,	TP_SULI,
		TP_SEME,	TP_SONA,	TP_SOKO,	TP_SINPIN,
		TP_SINA,	TP_SITELEN,	TG(SUWI),	TP_SOWELI
	),
	[SEWI] = LAYOUT_ortho_4x4(
		_______,	_______,	TP_SEWI,	TP_SELI,
		_______,	_______,	_______,	_______,
		_______,	_______,	TP_SELO,	_______,
		_______,	_______,	_______,	_______
	),
	[SUWI] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	_______,
		_______,	_______,	_______,	_______,
		_______,	_______,	TP_SUNO,	TP_SUPA,
		_______,	_______,	TP_SUWI,	_______
	),
	[TAWA] = LAYOUT_ortho_4x4(
		_______,	TP_TAN,		TP_TELO,	TP_TONSI,
		_______,	_______,	_______,	_______,
		TP_TOMO,	TP_TENPO,	TP_TOKI,	_______,
		TP_TASO,	TP_TAWA,	TP_TU,		_______
	),
	[UTALA] = LAYOUT_ortho_4x4(
		_______,	_______,	_______,	_______,
		_______,	_______,	_______,	_______,
		_______,	TP_UNPA,	_______,	_______,
		_______,	TP_UTA,		TP_UTALA,	_______
	),
	[WILE] = LAYOUT_ortho_4x4(
		_______,	TP_WAWA,	TP_WEKA,	_______,
		_______,	_______,	_______,	TP_WALO,
		_______,	TP_WAN,		_______,	_______,
		TP_WASO,	_______,	_______,	TP_WILE
	)
};

const HSV basecolor = (HSV){HSV_WHITE};
const HSV layer1color = (HSV){HSV_YELLOW};
const HSV layer2color = (HSV){HSV_BLUE};
const HSV othercolor = (HSV){HSV_YELLOW};

const rgblight_segment_t PROGMEM rgblayer_base[] = RGBLIGHT_LAYER_SEGMENTS(
	{0, 16, basecolor.h, basecolor.s, basecolor.v}
);
const rgblight_segment_t PROGMEM rgblayer_sp1[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 3, layer2color.h, layer2color.s, layer2color.v},
	{5, 11, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_sp2[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 15, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_a[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 3, layer1color.h, layer1color.s, layer1color.v},
	{6, 2, layer1color.h, layer1color.s, layer1color.v},
	{9, 1, layer1color.h, layer1color.s, layer1color.v},
	{11, 5, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_e[] = RGBLIGHT_LAYER_SEGMENTS(
	{2, 1, layer1color.h, layer1color.s, layer1color.v},
	{9, 1, layer1color.h, layer1color.s, layer1color.v},
	{11, 2, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_ike[] = RGBLIGHT_LAYER_SEGMENTS(
	{3, 1, layer1color.h, layer1color.s, layer1color.v},
	{5, 1, layer1color.h, layer1color.s, layer1color.v},
	{7, 1, layer1color.h, layer1color.s, layer1color.v},
	{9, 1, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_jan[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 2, layer1color.h, layer1color.s, layer1color.v},
	{5, 1, layer1color.h, layer1color.s, layer1color.v},
	{10, 1, layer1color.h, layer1color.s, layer1color.v},
	{12, 1, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_kama[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 2, layer1color.h, layer1color.s, layer1color.v},
	{3, 1, othercolor.h, othercolor.s, othercolor.v},
	{5, 9, layer1color.h, layer1color.s, layer1color.v},
	{14, 1, othercolor.h, othercolor.s, othercolor.v},
	{15, 1, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_kili[] = RGBLIGHT_LAYER_SEGMENTS(
	{3, 1, layer2color.h, layer2color.s, layer2color.v},
	{9, 1, layer2color.h, layer2color.s, layer2color.v},
	{11, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_ku[] = RGBLIGHT_LAYER_SEGMENTS(
	{7, 1, layer2color.h, layer2color.s, layer2color.v},
	{14, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_la[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 1, layer1color.h, layer1color.s, layer1color.v},
	{2, 2, othercolor.h, othercolor.s, othercolor.v},
	{5, 3, layer1color.h, layer1color.s, layer1color.v},
	{9, 7, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_li[] = RGBLIGHT_LAYER_SEGMENTS(
	{3, 1, layer2color.h, layer2color.s, layer2color.v},
	{7, 1, layer2color.h, layer2color.s, layer2color.v},
	{9, 1, layer2color.h, layer2color.s, layer2color.v},
	{15, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_len[] = RGBLIGHT_LAYER_SEGMENTS(
	{2, 1, layer2color.h, layer2color.s, layer2color.v},
	{6, 1, layer2color.h, layer2color.s, layer2color.v},
	{9, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_mi[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 3, layer1color.h, layer1color.s, layer1color.v},
	{5, 5, layer1color.h, layer1color.s, layer1color.v},
	{10, 1, othercolor.h, othercolor.s, othercolor.v},
	{11, 5, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_monsi[] = RGBLIGHT_LAYER_SEGMENTS(
	{3, 1, layer2color.h, layer2color.s, layer2color.v},
	{9, 2, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_nimi[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 3, layer1color.h, layer1color.s, layer1color.v},
	{8, 6, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_o[] = RGBLIGHT_LAYER_SEGMENTS(
	{6, 2, layer1color.h, layer1color.s, layer1color.v},
	{9, 3, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_pi[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 2, layer1color.h, layer1color.s, layer1color.v},
	{3, 1, othercolor.h, othercolor.s, othercolor.v},
	{6, 4, layer1color.h, layer1color.s, layer1color.v},
	{10, 1, othercolor.h, othercolor.s, othercolor.v},
	{11, 2, layer1color.h, layer1color.s, layer1color.v},
	{14, 1, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_pini[] = RGBLIGHT_LAYER_SEGMENTS(
	{3, 1, layer2color.h, layer2color.s, layer2color.v},
	{7, 1, layer2color.h, layer2color.s, layer2color.v},
	{11, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_poka[] = RGBLIGHT_LAYER_SEGMENTS(
	{6, 1, layer2color.h, layer2color.s, layer2color.v},
	{9, 2, layer2color.h, layer2color.s, layer2color.v},
	{15, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_sina[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 1, layer1color.h, layer1color.s, layer1color.v},
	{2, 1, othercolor.h, othercolor.s, othercolor.v},
	{3, 1, layer1color.h, layer1color.s, layer1color.v},
	{5, 9, layer1color.h, layer1color.s, layer1color.v},
	{14, 1, othercolor.h, othercolor.s, othercolor.v},
	{15, 1, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_sewi[] = RGBLIGHT_LAYER_SEGMENTS(
	{2, 2, layer2color.h, layer2color.s, layer2color.v},
	{10, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_suwi[] = RGBLIGHT_LAYER_SEGMENTS(
	{10, 2, layer2color.h, layer2color.s, layer2color.v},
	{14, 1, layer2color.h, layer2color.s, layer2color.v}
);
const rgblight_segment_t PROGMEM rgblayer_tawa[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 3, layer1color.h, layer1color.s, layer1color.v},
	{8, 3, layer1color.h, layer1color.s, layer1color.v},
	{12, 3, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_utala[] = RGBLIGHT_LAYER_SEGMENTS(
	{9, 1, layer1color.h, layer1color.s, layer1color.v},
	{13, 2, layer1color.h, layer1color.s, layer1color.v}
);
const rgblight_segment_t PROGMEM rgblayer_wile[] = RGBLIGHT_LAYER_SEGMENTS(
	{1, 2, layer1color.h, layer1color.s, layer1color.v},
	{7, 1, layer1color.h, layer1color.s, layer1color.v},
	{9, 1, layer1color.h, layer1color.s, layer1color.v},
	{12, 1, layer1color.h, layer1color.s, layer1color.v},
	{15, 1, layer1color.h, layer1color.s, layer1color.v}
);

const rgblight_segment_t* const PROGMEM my_rgb_layers[] = RGBLIGHT_LAYERS_LIST(
	rgblayer_base,
	rgblayer_sp1,
	rgblayer_sp2,
	rgblayer_a,
	rgblayer_e,
	rgblayer_ike,
	rgblayer_jan,
	rgblayer_kama,
		rgblayer_kili,
		rgblayer_ku,
	rgblayer_la,
		rgblayer_li,
		rgblayer_len,
	rgblayer_mi,
		rgblayer_monsi,
	rgblayer_nimi,
	rgblayer_o,
	rgblayer_pi,
		rgblayer_pini,
		rgblayer_poka,
	rgblayer_sina,
		rgblayer_sewi,
		rgblayer_suwi,
	rgblayer_tawa,
	rgblayer_utala,
	rgblayer_wile
);

void keyboard_post_init_user(void) {
	rgblight_layers = my_rgb_layers;
}

layer_state_t default_layer_state_set_user(layer_state_t state) {
	rgblight_set_layer_state(0, true);
	return state;
}

layer_state_t layer_state_set_user(layer_state_t state) {
	rgblight_set_layer_state(0, get_highest_layer(state) == BASE);
	rgblight_set_layer_state(1, layer_state_cmp(state, SP1));
	rgblight_set_layer_state(2, layer_state_cmp(state, SP2));
	rgblight_set_layer_state(3, layer_state_cmp(state, A));
	rgblight_set_layer_state(4, layer_state_cmp(state, E));
	rgblight_set_layer_state(5, layer_state_cmp(state, IKE));
	rgblight_set_layer_state(6, layer_state_cmp(state, JAN));
	rgblight_set_layer_state(7, layer_state_cmp(state, KAMA));
	rgblight_set_layer_state(8, layer_state_cmp(state, KILI));
	rgblight_set_layer_state(9, layer_state_cmp(state, KU));
	rgblight_set_layer_state(10, layer_state_cmp(state, LA));
	rgblight_set_layer_state(11, layer_state_cmp(state, LI));
	rgblight_set_layer_state(12, layer_state_cmp(state, LEN));
	rgblight_set_layer_state(13, layer_state_cmp(state, MI));
	rgblight_set_layer_state(14, layer_state_cmp(state, MONSI));
	rgblight_set_layer_state(15, layer_state_cmp(state, NIMI));
	rgblight_set_layer_state(16, layer_state_cmp(state, O));
	rgblight_set_layer_state(17, layer_state_cmp(state, PI));
	rgblight_set_layer_state(18, layer_state_cmp(state, PINI));
	rgblight_set_layer_state(19, layer_state_cmp(state, POKA));
	rgblight_set_layer_state(20, layer_state_cmp(state, SINA));
	rgblight_set_layer_state(21, layer_state_cmp(state, SEWI));
	rgblight_set_layer_state(22, layer_state_cmp(state, SUWI));
	rgblight_set_layer_state(23, layer_state_cmp(state, TAWA));
	rgblight_set_layer_state(24, layer_state_cmp(state, UTALA));
	rgblight_set_layer_state(25, layer_state_cmp(state, WILE));
	return state;
}


bool last_pressed_word = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	char *typevalue = "";
	switch (keycode) {
		case TP_A:	typevalue = strdup("a ");	break;
		case TP_ALE:	typevalue = strdup("ale ");	break;
		case TP_ALI:	typevalue = strdup("ali ");	break;
		case TP_AKESI:	typevalue = strdup("akesi ");	break;
		case TP_ALA:	typevalue = strdup("ala ");	break;
		case TP_ANPA:	typevalue = strdup("anpa ");	break;
		case TP_APEJA:	typevalue = strdup("apeja ");	break;
		case TP_ALASA:	typevalue = strdup("alasa ");	break;
		case TP_ANTE:	typevalue = strdup("ante ");	break;
		case TP_ANU:	typevalue = strdup("anu ");	break;
		case TP_AWEN:	typevalue = strdup("awen ");	break;
		case TP_E:	typevalue = strdup("e ");	break;
		case TP_EN:	typevalue = strdup("en ");	break;
		case TP_EPIKU:	typevalue = strdup("epiku ");	break;
		case TP_ESUN:	typevalue = strdup("esun ");	break;
		case TP_IKE:	typevalue = strdup("ike ");	break;
		case TP_IJO:	typevalue = strdup("ijo ");	break;
		case TP_ILO:	typevalue = strdup("ilo ");	break;
		case TP_INSA:	typevalue = strdup("insa ");	break;
		case TP_JAKI:	typevalue = strdup("jaki ");	break;
		case TP_JELO:	typevalue = strdup("jelo ");	break;
		case TP_JAN:	typevalue = strdup("jan ");	break;
		case TP_JO:	typevalue = strdup("jo ");	break;
		case TP_JASIMA:	typevalue = strdup("jasima ");	break;
		case TP_KALA:	typevalue = strdup("kala ");	break;
		case TP_KEN:	typevalue = strdup("ken ");	break;
		case TP_KILI:	typevalue = strdup("kili ");	break;
		case TP_KIN:	typevalue = strdup("kin ");	break;
		case TP_KIPISI:	typevalue = strdup("kipisi ");	break;
		case TP_KIJETESANTAKALU:	typevalue = strdup("kijetesantakalu ");	break;
		case TP_KAMA:	typevalue = strdup("kama ");	break;
		case TP_KULUPU:	typevalue = strdup("kulupu ");	break;
		case TP_KALAMA:	typevalue = strdup("kalama ");	break;
		case TP_KON:	typevalue = strdup("kon ");	break;
		case TP_KO:	typevalue = strdup("ko ");	break;
		case TP_KEPEKEN:	typevalue = strdup("kepeken ");	break;
		case TP_KASI:	typevalue = strdup("kasi ");	break;
		case TP_KUTE:	typevalue = strdup("kute ");	break;
		case TP_KU:	typevalue = strdup("ku ");	break;
		case TP_KULE:	typevalue = strdup("kule ");	break;
		case TP_KIWEN:	typevalue = strdup("kiwen ");	break;
		case TP_LAPE:	typevalue = strdup("lape ");	break;
		case TP_LOJE:	typevalue = strdup("loje ");	break;
		case TP_LUKA:	typevalue = strdup("luka ");	break;
		case TP_LA:	typevalue = strdup("la ");	break;
		case TP_LI:	typevalue = strdup("li ");	break;
		case TP_LUKIN:	typevalue = strdup("lukin ");	break;
		case TP_LON:	typevalue = strdup("lon ");	break;
		case TP_LUPA:	typevalue = strdup("lupa ");	break;
		case TP_LASO:	typevalue = strdup("laso ");	break;
		case TP_LETE:	typevalue = strdup("lete ");	break;
		case TP_LIPU:	typevalue = strdup("lipu ");	break;
		case TP_LAWA:	typevalue = strdup("lawa ");	break;
		case TP_LILI:	typevalue = strdup("lili ");	break;
		case TP_LINLUWI:	typevalue = strdup("linluwi ");	break;
		case TP_LINJA:	typevalue = strdup("linja ");	break;
		case TP_LEN:	typevalue = strdup("len ");	break;
		case TP_LEKO:	typevalue = strdup("leko ");	break;
		case TP_LANPAN:	typevalue = strdup("lanpan ");	break;
		case TP_MAMA:	typevalue = strdup("mama ");	break;
		case TP_MELI:	typevalue = strdup("meli ");	break;
		case TP_MANI:	typevalue = strdup("mani ");	break;
		case TP_MIJE:	typevalue = strdup("mije ");	break;
		case TP_MOKU:	typevalue = strdup("moku ");	break;
		case TP_MOLI:	typevalue = strdup("moli ");	break;
		case TP_MI:	typevalue = strdup("mi ");	break;
		case TP_MUN:	typevalue = strdup("mun ");	break;
		case TP_MESO:	typevalue = strdup("meso ");	break;
		case TP_MUSI:	typevalue = strdup("musi ");	break;
		case TP_MUTE:	typevalue = strdup("mute ");	break;
		case TP_MU:	typevalue = strdup("mu ");	break;
		case TP_MA:	typevalue = strdup("ma ");	break;
		case TP_MISIKEKE:	typevalue = strdup("misikeke ");	break;
		case TP_MONSUTA:	typevalue = strdup("monsuta ");	break;
		case TP_MONSI:	typevalue = strdup("monsi ");	break;
		case TP_NANPA:	typevalue = strdup("nanpa ");	break;
		case TP_NENA:	typevalue = strdup("nena ");	break;
		case TP_NI:	typevalue = strdup("ni ");	break;
		case TP_NAMAKO:	typevalue = strdup("namako ");	break;
		case TP_NIMI:	typevalue = strdup("nimi ");	break;
		case TP_NOKA:	typevalue = strdup("noka ");	break;
		case TP_NASIN:	typevalue = strdup("nasin ");	break;
		case TP_NASA:	typevalue = strdup("nasa ");	break;
		case TP_N:	typevalue = strdup("n ");	break;
		case TP_OKO:	typevalue = strdup("oko ");	break;
		case TP_OLIN:	typevalue = strdup("olin ");	break;
		case TP_ONA:	typevalue = strdup("ona ");	break;
		case TP_O:	typevalue = strdup("o ");	break;
		case TP_OPEN:	typevalue = strdup("open ");	break;
		case TP_PAN:	typevalue = strdup("pan ");	break;
		case TP_PAKE:	typevalue = strdup("pake ");	break;
		case TP_PAKALA:	typevalue = strdup("pakala ");	break;
		case TP_PALI:	typevalue = strdup("pali ");	break;
		case TP_PIMEJA:	typevalue = strdup("pimeja ");	break;
		case TP_PANA:	typevalue = strdup("pana ");	break;
		case TP_PI:	typevalue = strdup("pi ");	break;
		case TP_PALISA:	typevalue = strdup("palisa ");	break;
		case TP_PU:	typevalue = strdup("pu ");	break;
		case TP_PINI:	typevalue = strdup("pini ");	break;
		case TP_PILIN:	typevalue = strdup("pilin ");	break;
		case TP_PIPI:	typevalue = strdup("pipi ");	break;
		case TP_POKI:	typevalue = strdup("poki ");	break;
		case TP_PONA:	typevalue = strdup("pona ");	break;
		case TP_POKA:	typevalue = strdup("poka ");	break;
		case TP_POWE:	typevalue = strdup("powe ");	break;
		case TP_SAMA:	typevalue = strdup("sama ");	break;
		case TP_SIN:	typevalue = strdup("sin ");	break;
		case TP_SIJELO:	typevalue = strdup("sijelo ");	break;
		case TP_SIKE:	typevalue = strdup("sike ");	break;
		case TP_SULI:	typevalue = strdup("suli ");	break;
		case TP_SEME:	typevalue = strdup("seme ");	break;
		case TP_SONA:	typevalue = strdup("sona ");	break;
		case TP_SOKO:	typevalue = strdup("soko ");	break;
		case TP_SINPIN:	typevalue = strdup("sinpin ");	break;
		case TP_SINA:	typevalue = strdup("sina ");	break;
		case TP_SITELEN:	typevalue = strdup("sitelen ");	break;
		case TP_SOWELI:	typevalue = strdup("soweli ");	break;
		case TP_SEWI:	typevalue = strdup("sewi ");	break;
		case TP_SELI:	typevalue = strdup("seli ");	break;
		case TP_SELO:	typevalue = strdup("selo ");	break;
		case TP_SUNO:	typevalue = strdup("suno ");	break;
		case TP_SUPA:	typevalue = strdup("supa ");	break;
		case TP_SUWI:	typevalue = strdup("suwi ");	break;
		case TP_TAN:	typevalue = strdup("tan ");	break;
		case TP_TELO:	typevalue = strdup("telo ");	break;
		case TP_TONSI:	typevalue = strdup("tonsi ");	break;
		case TP_TOMO:	typevalue = strdup("tomo ");	break;
		case TP_TENPO:	typevalue = strdup("tenpo ");	break;
		case TP_TOKI:	typevalue = strdup("toki ");	break;
		case TP_TASO:	typevalue = strdup("taso ");	break;
		case TP_TAWA:	typevalue = strdup("tawa ");	break;
		case TP_TU:	typevalue = strdup("tu ");	break;
		case TP_UNPA:	typevalue = strdup("unpa ");	break;
		case TP_UTA:	typevalue = strdup("uta ");	break;
		case TP_UTALA:	typevalue = strdup("utala ");	break;
		case TP_WAWA:	typevalue = strdup("wawa ");	break;
		case TP_WEKA:	typevalue = strdup("weka ");	break;
		case TP_WALO:	typevalue = strdup("walo ");	break;
		case TP_WAN:	typevalue = strdup("wan ");	break;
		case TP_WASO:	typevalue = strdup("waso ");	break;
		case TP_WILE:	typevalue = strdup("wile ");	break;

		case TP_DOT:	typevalue = strdup(". ");	break;
		case TP_COMM:	typevalue = strdup(", ");	break;
		case TP_QUOT:	typevalue = strdup("\" ");	break;
		case TP_EXCL:	typevalue = strdup("! ");	break;
		case TP_QUES:	typevalue = strdup("? ");	break;
		case TP_DASH:	typevalue = strdup("- ");	break;
		case TP_UNDR:	typevalue = strdup("_ ");	break;
		case TP_SCLN:	typevalue = strdup("; ");	break;
		case TP_CLON:	typevalue = strdup(": ");	break;
		case TP_LBRC:	typevalue = strdup("[ ");	break;
		case TP_RBRC:	typevalue = strdup("] ");	break;

		case TPL_A:	typevalue = strdup("a");	break;
		case TPL_E:	typevalue = strdup("e");	break;
		case TPL_I:	typevalue = strdup("i");	break;
		case TPL_J:	typevalue = strdup("j");	break;
		case TPL_K:	typevalue = strdup("k");	break;
		case TPL_L:	typevalue = strdup("l");	break;
		case TPL_M:	typevalue = strdup("m");	break;
		case TPL_N:	typevalue = strdup("n");	break;
		case TPL_O:	typevalue = strdup("o");	break;
		case TPL_P:	typevalue = strdup("p");	break;
		case TPL_S:	typevalue = strdup("s");	break;
		case TPL_T:	typevalue = strdup("t");	break;
		case TPL_U:	typevalue = strdup("u");	break;
		case TPL_W:	typevalue = strdup("w");	break;
	}
	switch (keycode) {
		case TP_A:
		case TP_ALE:
		case TP_ALI:
		case TP_AKESI:
		case TP_ALA:
		case TP_ANPA:
		case TP_APEJA:
		case TP_ALASA:
		case TP_ANTE:
		case TP_ANU:
		case TP_AWEN:
		case TP_E:
		case TP_EN:
		case TP_EPIKU:
		case TP_ESUN:
		case TP_IKE:
		case TP_IJO:
		case TP_ILO:
		case TP_INSA:
		case TP_JAKI:
		case TP_JELO:
		case TP_JAN:
		case TP_JO:
		case TP_JASIMA:
		case TP_KALA:
		case TP_KEN:
		case TP_KILI:
		case TP_KIN:
		case TP_KIPISI:
		case TP_KIJETESANTAKALU:
		case TP_KAMA:
		case TP_KULUPU:
		case TP_KALAMA:
		case TP_KON:
		case TP_KO:
		case TP_KEPEKEN:
		case TP_KASI:
		case TP_KUTE:
		case TP_KU:
		case TP_KULE:
		case TP_KIWEN:
		case TP_LAPE:
		case TP_LOJE:
		case TP_LUKA:
		case TP_LA:
		case TP_LI:
		case TP_LUKIN:
		case TP_LON:
		case TP_LUPA:
		case TP_LASO:
		case TP_LETE:
		case TP_LIPU:
		case TP_LAWA:
		case TP_LILI:
		case TP_LINLUWI:
		case TP_LINJA:
		case TP_LEN:
		case TP_LEKO:
		case TP_LANPAN:
		case TP_MAMA:
		case TP_MELI:
		case TP_MANI:
		case TP_MIJE:
		case TP_MOKU:
		case TP_MOLI:
		case TP_MI:
		case TP_MUN:
		case TP_MESO:
		case TP_MUSI:
		case TP_MUTE:
		case TP_MU:
		case TP_MA:
		case TP_MISIKEKE:
		case TP_MONSUTA:
		case TP_MONSI:
		case TP_NANPA:
		case TP_NENA:
		case TP_NI:
		case TP_NAMAKO:
		case TP_NIMI:
		case TP_NOKA:
		case TP_NASIN:
		case TP_NASA:
		case TP_N:
		case TP_OKO:
		case TP_OLIN:
		case TP_ONA:
		case TP_O:
		case TP_OPEN:
		case TP_PAN:
		case TP_PAKE:
		case TP_PAKALA:
		case TP_PALI:
		case TP_PIMEJA:
		case TP_PANA:
		case TP_PI:
		case TP_PALISA:
		case TP_PU:
		case TP_PINI:
		case TP_PILIN:
		case TP_PIPI:
		case TP_POKI:
		case TP_PONA:
		case TP_POKA:
		case TP_POWE:
		case TP_SAMA:
		case TP_SIN:
		case TP_SIJELO:
		case TP_SIKE:
		case TP_SULI:
		case TP_SEME:
		case TP_SONA:
		case TP_SOKO:
		case TP_SINPIN:
		case TP_SINA:
		case TP_SITELEN:
		case TP_SOWELI:
		case TP_SEWI:
		case TP_SELI:
		case TP_SELO:
		case TP_SUNO:
		case TP_SUPA:
		case TP_SUWI:
		case TP_TAN:
		case TP_TELO:
		case TP_TONSI:
		case TP_TOMO:
		case TP_TENPO:
		case TP_TOKI:
		case TP_TASO:
		case TP_TAWA:
		case TP_TU:
		case TP_UNPA:
		case TP_UTA:
		case TP_UTALA:
		case TP_WAWA:
		case TP_WEKA:
		case TP_WALO:
		case TP_WAN:
		case TP_WASO:
		case TP_WILE:
			if (record->event.pressed) {
				send_string(typevalue);
				last_pressed_word = true;
				layer_clear();
				return false;
			}
			break;
		case TP_DOT:
		case TP_COMM:
		case TP_QUOT:
		case TP_EXCL:
		case TP_QUES:
		case TP_DASH:
		case TP_UNDR:
		case TP_SCLN:
		case TP_CLON:
		case TP_LBRC:
		case TP_RBRC:
			if (record->event.pressed) {
				if (last_pressed_word) {
					tap_code(KC_BACKSPACE);
				}
				send_string(typevalue);
				last_pressed_word = false;
				return false;
			}
			break;
		case TPL_A:
		case TPL_E:
		case TPL_I:
		case TPL_J:
		case TPL_K:
		case TPL_L:
		case TPL_M:
		case TPL_N:
		case TPL_O:
		case TPL_P:
		case TPL_S:
		case TPL_T:
		case TPL_U:
		case TPL_W:
			if (record->event.pressed) {
				send_string(typevalue);
				last_pressed_word = false;
				return false;
			}
			break;
		case LT(SP1,TP_BSP):
			if (record->tap.count && record->event.pressed) {
				register_code(KC_LCTL);
				tap_code(KC_BSPC);
				unregister_code(KC_LCTL);
				last_pressed_word = false;
				layer_clear();
				return false;
			}
			break;
		case LT(SP2,TP_ENT):
			if (record->tap.count && record->event.pressed) {
				tap_code(KC_ENT);
				last_pressed_word = false;
				layer_clear();
				return false;
			}
			break;
		case TP_SPC:
			if (record->event.pressed) {
				tap_code(KC_SPACE);
				last_pressed_word = false;
				return false;
			}
			break;
		case TP_BSPC:
			if (record->event.pressed) {
				tap_code(KC_BACKSPACE);
				last_pressed_word = false;
				return false;
			}
			break;
	}
	return true;
};
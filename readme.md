# Toki Pona BM16S Keymap

A [QMK](https://qmk.fm/) keymap for the [Kprepublic BM16S](https://kprepublic.com/collections/bm16/products/bm16s-16-keys-custom-mechanical-keyboard-pcb-plate-programmed-numpad-layouts-qmk-firmware-with-rgb-switch-leds-choc-switch) that turns it into a fully-functional Toki Pona Keyboard.



The layout is heavily based on [the Toki Pona Android keyboard by Timeoopochin](https://github.com/timeopochin/tokiponakeyboard), but is adapted to use only 16 keys.

Uses the per-key RGB lighting to indicate which keys do what on a given layer.

[Video Demo](https://www.youtube.com/watch?v=US7oOCE-dv8)

If you'd like to use the keyboard, here's how it works, and how to figure out how to type a word:

- The default layer has all keys illuminated in white
- Pressing a letter key will bring up a layer with the words starting with that letter (illuminated in yellow)
- Pressing a key on that layer will either:
  - Type the word that has that letter next (O, P => "open")
  - Type the word that has that letter coming up but not next (if there are multiple valid candidates) (A, I => "ali")
  - If it is the same key you pressed to get to that letter, type either the word the letter *is* (in the case of a, e, and o) or the most common word for that letter (J, J => "jan")
  - Bring up a second layer which functions the same as the first, except it highlights new words in blue (K, I, P => "kipisi")
- Pressing the top-leftmost key will erase the last word (as though you'd pressed `ctrl+backspace`)
  - Holding it will instead bring up the punctuation layer (which you can see detailed below as `Special Layer 1`)
- Pressing the key below it will enter (as though you'd pressed the `enter` key)
  - Holding it will instead bring up the manual writing layer (which you can see detailed below as `Special Layer 2`)
    - Pressing any letter key on this layer will enter that letter (useful for writing names)

Additionally, if you'd like, there's also a detailed list of every layer in the last section of this readme document.

## Build Instructions

For the tokiponists out there who don't know much about mechanical keyboards, here's what you'll need to build this (no soldering necessary).

Parts (with costs, in USD, at time of writing):

- [The PCB](https://kprepublic.com/collections/bm16/products/bm16s-16-keys-custom-mechanical-keyboard-pcb-plate-programmed-numpad-layouts-qmk-firmware-with-rgb-switch-leds-choc-switch) - $21.60
- [Switches](https://kprepublic.com/products/kailh-low-profile-choc-switch-half-high-ultrathin-rgb-swithes-for-backlit-mechanical-keyboard-dark-yellow-burnt-orange-pale-blue?variant=12332287721516) - $5.80 * 2
  - Pick whichever color you prefer
    - Burnt Orange: tactile (similar to most membrane keyboards you may be used to)
    - Dark Yellow: linear (not tactile or clicky)
    - Pale Blue: clicky
- [Keycaps](https://kprepublic.com/products/kailh-low-profile-keycap-set-for-kailh-low-profile-swtich-abs-doubleshot-ultra-thin-keycap-for-low-profile-white-brown-red) - $14.90
  - You can substitute [MBK Glow](https://www.littlekeyboards.com/products/mbk-glow-keycaps)s - $54.99 if you want something that looks and feels nicer, but will still let the LEDs shine through (but holy hell look at that price difference)

Instructions:

- Insert the switches into the PCB
  - The PCB has hotswap sockets, so you don't need to solder them in
- Insert the keycaps into the switches as in the image below (use whatever for the top left two):

![keyboard](keyboard.jpg)
- [Download the built keymap](https://gitlab.com/MayESchaefer/toki-pona-bm16s-keymap/uploads/4fd34f0178d7fd7f85805b3b458ff158/kprepublic_bm16s_tokipona.hex) (or build it yourself if you want I guess)
- [Flash the keymap](https://docs.qmk.fm/#/newbs_flashing?id=flashing-your-keyboard-with-qmk-toolbox)
  - You'll need to put the keyboard into DFU mode by plugging it in with the top left key pressed (should be obvious if it works, the LEDs will be off)

## Layout

| Base Layer                          |            |             |            |
| ----------------------------------- | ---------- | ----------- | ---------- |
| Backspace (or SP1 layer while held) | A layer    | E layer     | Ike layer  |
| Enter (or SP2 layer while held)     | Jan layer  | Kama layer  | La layer   |
| Mi layer                            | Nimi layer | O layer     | Pi layer   |
| Sina layer                          | Tawa layer | Utala layer | Wile layer |

| Special Layer 1  |      |      |                                                              |
| ---------------- | ---- | ---- | ------------------------------------------------------------ |
|                  | .    | ,    | "                                                            |
|                  | !    | ?    | -                                                            |
| Space            | _    | ;    | :                                                            |
| Normal Backspace | [    | ]    | Shift (applies to the next key you press, no need to hold down) |

| Special Layer 2 |            |            |            |
| --------------- | ---------- | ---------- | ---------- |
| Space           | a (letter) | e (letter) | i (letter) |
|                 | j (letter) | k (letter) | l (letter) |
| m (letter)      | n (letter) | o (letter) | p (letter) |
| s (letter)      | t (letter) | u (letter) | w (letter) |

| A Layer |      |       |       |
| ------- | ---- | ----- | ----- |
|         | a    | ale   | ali   |
|         |      | akesi | ala   |
|         | anpa |       | apeja |
| alasa   | ante | anu   | awen  |

| E Layer |      |      |       |
| ------- | ---- | ---- | ----- |
|         |      | e    |       |
|         |      |      |       |
|         | en   |      | epiku |
| esun    |      |      |       |

| Ike Layer |      |      |      |
| --------- | ---- | ---- | ---- |
|           |      |      | ike  |
|           | ijo  |      | ilo  |
|           | insa |      |      |
|           |      |      |      |

| Jan Layer |      |      |      |
| --------- | ---- | ---- | ---- |
|           | jaki | jelo |      |
|           | jan  |      |      |
|           |      | jo   |      |
| jasima    |      |      |      |

| Kama Layer |                 |          |            |
| ---------- | --------------- | -------- | ---------- |
|            | kala            | ken      | Kili layer |
|            | kijetesantakalu | kama     | kulupu     |
| kalama     | kon             | ko       | kepeken    |
| kasi       | kute            | Ku layer | kiwen      |

| Kili Layer |      |      |        |
| ---------- | ---- | ---- | ------ |
|            |      |      | kili   |
|            |      |      |        |
|            | kin  |      | kipisi |
|            |      |      |        |

| Ku Layer |      |      |      |
| -------- | ---- | ---- | ---- |
|          |      |      |      |
|          |      |      | kule |
|          |      |      |      |
|          |      | ku   |      |

| La Layer |       |           |          |
| -------- | ----- | --------- | -------- |
|          | lape  | Len layer | Li layer |
|          | loje  | luka      | la       |
|          | lukin | lon       | lupa     |
| laso     | lete  | lipu      | lawa     |

| Li Layer |       |      |         |
| -------- | ----- | ---- | ------- |
|          |       |      | li      |
|          |       |      | lili    |
|          | linja |      |         |
|          |       |      | linluwi |

| Len Layer |        |      |      |
| --------- | ------ | ---- | ---- |
|           |        | len  |      |
|           |        | leko |      |
|           | lanpan |      |      |
|           |        |      |      |

| Mi Layer |      |             |      |
| -------- | ---- | ----------- | ---- |
|          | mama | meli        | mani |
|          | mije | moku        | moli |
| mi       | mun  | Monsi layer | meso |
| musi     | mute | mu          | ma   |

| Monsi Layer |         |       |          |
| ----------- | ------- | ----- | -------- |
|             |         |       | misikeke |
|             |         |       |          |
|             | monsuta | monsi |          |
|             |         |       |          |

| Nimi Layer |       |      |       |
| ---------- | ----- | ---- | ----- |
|            | nanpa | nena | ni    |
|            |       |      |       |
| namako     | nimi  | noka | nasin |
| nasa       | n     |      |       |

| O Layer |      |      |      |
| ------- | ---- | ---- | ---- |
|         |      |      |      |
|         |      | oko  | olin |
|         | ona  | o    | open |
|         |      |      |      |

| Pi Layer |      |            |            |
| -------- | ---- | ---------- | ---------- |
|          | pan  | pake       | Pini layer |
|          |      | pakala     | pali       |
| pimeja   | pana | Poka layer | pi         |
| palisa   |      | pu         |            |

| Pini Layer |      |      |       |
| ---------- | ---- | ---- | ----- |
|            |      |      | pini  |
|            |      |      | pilin |
|            |      |      | pipi  |
|            |      |      |       |

| Poka Layer |      |      |      |
| ---------- | ---- | ---- | ---- |
|            |      |      |      |
|            |      | poki |      |
|            | pona | poka |      |
|            |      |      | powe |

| Sina Layer |         |            |        |
| ---------- | ------- | ---------- | ------ |
|            | sama    | Sewi layer | sin    |
|            | sijelo  | sike       | suli   |
| seme       | sona    | soko       | sinpin |
| sina       | sitelen | Suwi layer | soweli |

| Sewi Layer |      |      |      |
| ---------- | ---- | ---- | ---- |
|            |      | sewi | seli |
|            |      |      |      |
|            |      | selo |      |
|            |      |      |      |

| Suwi Layer |      |      |      |
| ---------- | ---- | ---- | ---- |
|            |      |      |      |
|            |      |      |      |
|            |      | suno | supa |
|            |      | suwi |      |

| Tawa Layer |       |      |       |
| ---------- | ----- | ---- | ----- |
|            | tan   | telo | tonsi |
|            |       |      |       |
| tomo       | tenpo | toki |       |
| taso       | tawa  | tu   |       |

| Utala Layer |      |       |      |
| ----------- | ---- | ----- | ---- |
|             |      |       |      |
|             |      |       |      |
|             | unpa |       |      |
|             | uta  | utala |      |

| Wile Layer |      |      |      |
| ---------- | ---- | ---- | ---- |
|            | wawa | weka |      |
|            |      |      | walo |
|            | wan  |      |      |
| waso       |      |      | wile |